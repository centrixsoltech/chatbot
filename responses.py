#coding=utf-8
from __future__ import unicode_literals
import uuid
import errno
import os
import sys
import tempfile
import run
from argparse import ArgumentParser
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut

geolocator = Nominatim(user_agent="Python")
import mysql.connector

# from flask import Flask, request, abort, send_from_directory
# from werkzeug.middleware.proxy_fix import ProxyFix

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    LineBotApiError, InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
    SourceUser, SourceGroup, SourceRoom,
    TemplateSendMessage, ConfirmTemplate, MessageAction,
    ButtonsTemplate, ImageCarouselTemplate, ImageCarouselColumn, URIAction,
    PostbackAction, DatetimePickerAction,
    CameraAction, CameraRollAction, LocationAction,
    CarouselTemplate, CarouselColumn, PostbackEvent,
    StickerMessage, StickerSendMessage, LocationMessage, LocationSendMessage,
    ImageMessage, VideoMessage, AudioMessage, FileMessage,
    UnfollowEvent, FollowEvent, JoinEvent, LeaveEvent, BeaconEvent,
    FlexSendMessage, BubbleContainer, ImageComponent, BoxComponent,
    TextComponent, SpacerComponent, IconComponent, ButtonComponent,
    SeparatorComponent, QuickReply, QuickReplyButton,
    ImageSendMessage, CarouselContainer)

# Display Welcome Message Card using Line's Flex message.
def welcomeResponse(languageCode):
    textsList=welcomeResponseTexts(languageCode)
    bubble = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://scontent.flhe2-1.fna.fbcdn.net/v/t1.0-9/61311063_294460598100966_8744406561134215168_n.png?_nc_cat=109&_nc_eui2=AeHOeTCy-L79gEmXjAUEmSxFu9QXXY1lSOICC1W-ODd2dY1CWBQHAeu5dSChjQKZw5OznX6cF8g6caEuSxKY6BJS5rp2hv6d_TWuj_fSMReBGFUUV9eA2KNtqsYF08KlrRY&_nc_ht=scontent.flhe2-1.fna&oh=80504db992aa9d147696969870224228&oe=5D9777CA',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
            action=URIAction(uri='http://example.com', label='label')
        ),
        body=BoxComponent(
            layout='vertical',
            contents=[
                # title
                TextComponent(text=textsList[0], weight='bold', size='xl'),
                # review
                BoxComponent(
                    layout='baseline',
                    margin='md',
                    contents=[
                        IconComponent(size='sm',
                                      url='https://scontent.flhe2-1.fna.fbcdn.net/v/t1.0-9/60959386_294849928062033_5798305071934472192_n.png?_nc_cat=100&_nc_eui2=AeGIH3r9O5-1AWM5kG_CKSG6j7vU67oTwD4lZyJrfp6Zm-usgfgKE4qaLxcsJ-s30VGii_Z5PIA8LyfJp4caj4-aYsd6T8inmsy8cnzzxjcmzUPo3sxbBSYtT84ew5a6VIg&_nc_ht=scontent.flhe2-1.fna&oh=777fbb58c01a11944712cd244f265a54&oe=5D9ADCBA'),
                        TextComponent(text='02-026-3238', size='sm', color='#EA2225', margin='md',
                                      flex=0)
                    ]
                ),
                # info
                BoxComponent(
                    layout='vertical',
                    margin='lg',
                    spacing='sm',
                    contents=[
                        BoxComponent(
                            layout='baseline',
                            spacing='sm',
                            contents=[
                                TextComponent(
                                    text=textsList[1],
                                    color='#aaaaaa',
                                    size='sm',
                                    flex=1
                                ),
                                TextComponent(
                                    text=textsList[2],
                                    wrap=True,
                                    color='#666666',
                                    size='sm',
                                    flex=5
                                )
                            ],
                        ),
                        BoxComponent(
                            layout='baseline',
                            spacing='sm',
                            contents=[
                                TextComponent(
                                    text=textsList[3],
                                    color='#aaaaaa',
                                    size='sm',
                                    flex=1
                                ),
                                TextComponent(
                                    text="8:00 - 23:00",
                                    wrap=True,
                                    color='#666666',
                                    size='sm',
                                    flex=5,
                                ),
                            ],
                        ),
                    ],
                )
            ],
        ),
        footer=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                # callAction, separator, websiteAction
                SpacerComponent(size='sm'),
                # callAction
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color='#EA2225',
                    action=MessageAction(label=textsList[4], text=textsList[4]),
                ),
                # separator
                SeparatorComponent(),
                # websiteAction
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color='#000000',
                    action=MessageAction(label=textsList[5], text=textsList[5]),

                ),

                SeparatorComponent(),

                ButtonComponent(
                    style='secondary',
                    height='sm',
                    color='#666666',
                    action=MessageAction(label=textsList[6], text=textsList[6]),
                )
            ]
        ),
    )

    message = FlexSendMessage(alt_text="hello", contents=bubble)
    return message

def welcomeResponseTexts(languageCode):


    if languageCode=='th':
        textsList = ['ยินดีต้อนรับ', 'สถานที่', 'กรุงเทพประเทศไทย', 'เวลา','ค้นหาอย่างรวดเร็วตามสถานที่ตั้ง','ค้นหาด้วยการตั้งค่า','คำถามที่พบบ่อย']
    else:
        textsList=['Welcome To Drivemate','Place','Bangkok, Thailand','Time','Quick Search By Location','Search With Preference','FAQs']
    return textsList

def controlTexts(lang):
    if lang=='th':
        textList=['สำหรับรถยนต์เพิ่มเติมให้คลิกถัดไป','ก่อน','แสดงรถยนต์คันก่อนหน้า','การแสดง','ต่อไป','แสดงรถยนต์มากขึ้น']
    else:
        textList=['For more cars, click next.','Previous','Show previous cars','Showing','Next','Show more cars']
    return textList
#This message shows the controls of Next and Previous under the cars carousel.
def show_controls(num_cars, min, max, event):
    lang=getLangCode(event)
    textList=controlTexts(lang)
    message = TextSendMessage(
        text=textList[0],
        quick_reply=QuickReply(
            items=[

                QuickReplyButton(
                    image_url="https://scontent.flhe9-1.fna.fbcdn.net/v/t1.0-9/61419336_2317807791613469_4117670066768576512_n.jpg?_nc_cat=111&_nc_ht=scontent.flhe9-1.fna&oh=0e8c67a1a81e3637d658b232c43f91d0&oe=5D5F2C20",
                    action=PostbackAction(label=textList[1], data='Show Previous Cars Id ' + str(min) + "-" + str(max),
                                          display_text=textList[2])
                ),
                QuickReplyButton(

                    action=PostbackAction(label=textList[3] + str(min) + "-" + str(max) + ' of ' + str(num_cars),
                                          data='Showing ' + str(min) + "-" + str(max) + ' of ' + str(num_cars))
                ),
                QuickReplyButton(
                    image_url="https://scontent.flhe9-1.fna.fbcdn.net/v/t1.0-9/61553981_2317807798280135_4676507649133510656_n.jpg?_nc_cat=101&_nc_ht=scontent.flhe9-1.fna&oh=16d6a3416c091230adac51fa4d664fc5&oe=5D9EB18B",
                    action=PostbackAction(label=textList[4], data='Show Next Cars Id ' + str(min) + "-" + str(max),
                                          display_text=textList[4])
                )
            ]))
    return message

#This function returns the rent types quick reply message
def rentTypes(languageCode):
    textList=rentTypeTexts(languageCode)
    message = TextSendMessage(
        text=textList[0],
        quick_reply=QuickReply(
            items=[

                QuickReplyButton(
                    image_url="https://www.iconsdb.com/icons/preview/black/car-xxl.png",
                    action=MessageAction(label=textList[1], text=textList[1])
                ),
                QuickReplyButton(
                    image_url="https://cdn0.iconfinder.com/data/icons/driver-delivery-jobs-occupations-careers/276/occupation-05-013-512.png",
                    action=MessageAction(label=textList[2], text=textList[2])
                ),
                QuickReplyButton(
                    image_url="https://cdn2.iconfinder.com/data/icons/automobile-repair-service/512/mercedes-car-sedan-auto-front-view-512.png",
                    action=MessageAction(label=textList[3], text=textList[3])
                )
            ]))
    return message

def rentTypeTexts(languageCode):
    if languageCode=='th':
        textList=['โปรดระบุประเภทการเช่าของคุณ','รถเช่า','เช่าพร้อมคนขับ','ค่าเช่ารายเดือน']
    else:
        textList=['Please specify your rental type.','Car Rental','Rental with Driver','Monthly Rental']
    return textList


# this function returns the locations quick replies.
def locations(languageCode):
    textList=locationsTexts(languageCode)
    message = TextSendMessage(
        text=textList[0],
        quick_reply=QuickReply(
            items=[

                QuickReplyButton(
                    action=LocationAction(label=textList[1])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[2], text=textList[2])
                    #action=CameraRollAction(label="Upload Documents")
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[3], text=textList[3])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[4], text=textList[4])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[5], text=textList[5])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[6], text=textList[6])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[7], text=textList[7])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[8], text=textList[8])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[9], text=textList[9])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[10], text=textList[10])
                )
            ]))
    return message

def locationsTexts(languageCode):
    if languageCode=='th':
        textList=['โปรดระบุสถานที่รับรถของคุณ','แบ่งปันตำแหน่ง','กรุงเทพมหานคร','ชลบุรี','เชียงใหม่','เชียงราย','ลำปาง','กระบี่','ภูเก็ต','สิงห์บุรี','สุราษฎร์ธานี']
    else:
        textList=['Please specify your car pickup location.','Share location','Bangkok','Chon Buri','Chiang Mai','Chiang Rai','Lampang','Krabi','Phuket','Sing Buri','Surat Thani']
    return textList

def faqTexts(langCode):
    print("******IN FAQ TEXTS")
    if langCode == 'th':
        textList=['โปรดระบุการแปลงหรือพิมพ์ของคุณเอง',
 'ราคารถยนต์','ฉันจะกำหนดราคารถยนต์ได้อย่างไร','วิธีการชำระเงิน','มีวิธีการชำระเงินแบบใดบ้าง',
 'เงินฝากความปลอดภัย','คุณต้องการเงินประกันหรือไม่?',
'รถกระบะ / กลับ','ฉันจะรับ / คืนรถได้อย่างไร',
"นโยบายการประกันภัย",'นโยบายการประกันของคุณคืออะไร?',
'การยกเลิกการเดินทาง','ต้องทำอย่างไรหากฉันต้องการยกเลิกการเดินทาง',
'ค่าธรรมเนียมเชื้อเพลิง','ฉันต้องรับผิดชอบค่าน้ำมันหรือไม่',
'รายการรถของฉัน','จะทำอย่างไรถ้าฉันมีรถให้เช่า',
'แพ็คเกจประกันภัย','แพ็คเกจประกันราคาเท่าไหร่?',
'นโยบายการคืนเงิน','คุณจะคืนเงินค่ามัดจำความปลอดภัยของฉันเมื่อใด']
    else:
        textList=["Please specify your qustion or type your own.",
"Car Pricing", "How do I set the pricing of the car?",
"Payment Methods","What payment methods are available?",
"Security Deposit","Do you require a security deposit?",
"Pickup/Return","How do I pickup/return the car?",
"Insurance Policy","What is your insurance policy?",
"Trip Cancellation","What if I need to cancel a trip?",
"Fuel Charges","Am i responsible for paying for fuel?",
"Listing my Car","If I have a car to give for rent what should I do?",
"Insurance Packages","How much do various insurance packages cost?"
                  ]
    return textList
# this function returns the faqs quick replies.
def faqs(langCode):
    textList=faqTexts(langCode)
    print("******************")
    print(textList)
    message = TextSendMessage(
        text=textList[0],
        quick_reply=QuickReply(
            items=[

                QuickReplyButton(
                    action=MessageAction(label=textList[1], text=textList[2])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[3], text=textList[4])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[5], text=textList[6])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[7], text=textList[8])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[9], text=textList[10])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[11], text=textList[12])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[13], text=textList[14])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[15], text=textList[16])
                ),
                QuickReplyButton(
                    action=MessageAction(label=textList[17], text=textList[18])
                )

            ]))
    return message

# This function returns final options (end conversation, main menu, Faq) quick replies.
def finalOptions():
    message = TextSendMessage(
        text='I hope this helps, anything else?',
        quick_reply=QuickReply(
            items=[

                QuickReplyButton(
                    # image_url="https://www.iconsdb.com/icons/preview/black/car-xxl.png",
                    action=MessageAction(label="End Conversation", text="End Conversation")
                ),
                QuickReplyButton(
                    # image_url="https://cdn0.iconfinder.com/data/icons/driver-delivery-jobs-occupations-careers/276/occupation-05-013-512.png",
                    action=MessageAction(label="Go to Main Menu", text="Main Menu")
                ),
                QuickReplyButton(
                    # image_url="https://cdn2.iconfinder.com/data/icons/automobile-repair-service/512/mercedes-car-sedan-auto-front-view-512.png",
                    action=MessageAction(label="FAQs", text="FAQs")
                )
            ]))
    return message

# This function returns the flex message for start and end date postback messages for quickSearchByLocation
def timeduration(languageCode):
    textsList=timedurationTexts(languageCode)
    bubble = BubbleContainer(
        direction='ltr',
        hero='',
        body=BoxComponent(
            layout='vertical',
            contents=[
                # title

                # review
                BoxComponent(
                    layout='baseline',
                    margin='md',
                    contents=[
                        IconComponent(size='xxl', margin='xxl',
                                      url='https://scontent.flhe2-1.fna.fbcdn.net/v/t1.0-9/61614141_296008577946168_5215121214049091584_n.jpg?_nc_cat=110&_nc_eui2=AeHfH3IUzE_HwAcK2lT6B1VJ5vOvBUor5QVogqPI8KlHpCbJBz42eRpMD-GUDExVh7cy3cU5RYtH2Yc4qWLHCsnpDi9eRK6JFg-mktdrq6R9pwSGNN-vRd490zRgcrvjqP0&_nc_ht=scontent.flhe2-1.fna&oh=7d1a7be6b3ad802945f6efcc18b77358&oe=5D51CF58'),
                        TextComponent(text=textsList[0], size='md', color='#000000', margin='lg',
                                      flex=0)
                    ]
                ),

            ],
        ),
        footer=BoxComponent(
            layout='horizontal',
            spacing='sm',
            contents=[

                SpacerComponent(size='sm'),
                # callAction
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color='#EA2225',
                    action=DatetimePickerAction(label=textsList[1],
                                                data='startdatetime postback',
                                                mode='datetime')

                ),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color='#EA2225',
                    action=DatetimePickerAction(label=textsList[2],
                                                data='enddatetime postback',
                                                mode='datetime')

                )

            ]
        ),
    )

    message = FlexSendMessage(alt_text="hello", contents=bubble)
    return message

def timedurationTexts(languageCode):
    if languageCode=='th':
        textList=['โปรดระบุระยะเวลาการเดินทาง','วันที่เริ่มต้น','วันที่สิ้นสุด']
    else:
        textList=['Please specify the trip duration.','Start Date','End Date']
    return textList


# This function returns the flex message for start and end date postback messages for SearchWithPreference
def swptimeduration(languageCode):
    textList=timedurationTexts(languageCode)
    bubble = BubbleContainer(
        direction='ltr',
        hero='',
        body=BoxComponent(
            layout='vertical',
            contents=[
                # title

                # review
                BoxComponent(
                    layout='baseline',
                    margin='md',
                    contents=[
                        IconComponent(size='xxl', margin='xxl',
                                      url='https://scontent.flhe2-1.fna.fbcdn.net/v/t1.0-9/61614141_296008577946168_5215121214049091584_n.jpg?_nc_cat=110&_nc_eui2=AeHfH3IUzE_HwAcK2lT6B1VJ5vOvBUor5QVogqPI8KlHpCbJBz42eRpMD-GUDExVh7cy3cU5RYtH2Yc4qWLHCsnpDi9eRK6JFg-mktdrq6R9pwSGNN-vRd490zRgcrvjqP0&_nc_ht=scontent.flhe2-1.fna&oh=7d1a7be6b3ad802945f6efcc18b77358&oe=5D51CF58'),
                        TextComponent(text=textList[0], size='md', color='#000000', margin='lg',
                                      flex=0)
                    ]
                ),

            ],
        ),
        footer=BoxComponent(
            layout='horizontal',
            spacing='sm',
            contents=[

                SpacerComponent(size='sm'),
                # callAction
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color='#EA2225',
                    action=DatetimePickerAction(label=textList[1],
                                                data='swp startdatetime postback',
                                                mode='datetime')

                ),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color='#EA2225',
                    action=DatetimePickerAction(label=textList[2],
                                                data='swp enddatetime postback',
                                                mode='datetime')

                )

            ]
        ),
    )

    message = FlexSendMessage(alt_text="hello", contents=bubble)
    return message

# This function returns flex carousel of main search results.
def CarCard(id, distance,languageCode):
    textList=carCardList(languageCode)
    kms = str(distance)
    kms = kms[0:4]
    # import mysql.connector
    cnx = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
    cursor = cnx.cursor()
    get_reviews = getReviews(id)

    query = "SELECT car_car.id as id, IFNULL(car_car.price_per_day,'N/A') as price,IFNULL(car_car.location,'N/A') as location, IFNULL(car_car.regis_year,' ') as year,IFNULL(car_car.rating,'N/A') as rating, IFNULL(car_car.seats,'N/A') as seats, IFNULL(car_cargallery.photo,'N/A') as photo, IFNULL(master_car_carmodel.name,'N/A') as model, IFNULL(master_car_carbrand.name,'N/A') as brand, IFNULL(car_car.mileage,'N/A') as mileage,IFNULL(car_car.fuel_type,'N/A') as fuel_type, IFNULL(car_car.transmission,'N/A') as transmission , IFNULL(account_member.user_status,'') as user_status from car_car left join car_cargallery on car_car.id= car_cargallery.car_id left join master_car_carbrand on car_car.brand_id = master_car_carbrand.id left join master_car_carmodel on car_car.model_id= master_car_carmodel.id  left join account_member on car_car.owner_id= account_member.id where car_car.id='%s' LIMIT 1" % (
        id)
    cursor.execute(query)
    rv = cursor.fetchall()
    for result in rv:
        payment=textList[7]
        print("PAYMENT TYPE****************")
        print(str(result[12]))
        if str(result[11])=='private' or str(result[11])=='local':
            #payment='Cash/Credit Card'
            payment=textList[8]
        elif str(result[12])=='pro':
            payment=textList[7]
            #payment = 'Credit Card Only'
        print("CAR PHOTO **********************")
        print("https://www.drivemate.asia/media/" + str(result[6]))
        bubble = BubbleContainer(
            direction='ltr',
            hero=ImageComponent(
                url="https://www.drivemate.asia/media/" + str(result[6]),
                size='full',
                aspect_ratio='20:13',
                aspect_mode='cover',
                # action=URIAction(uri='http://example.com', label='label')
            ),
            body=BoxComponent(
                layout='vertical',
                contents=[
                    # title
                    TextComponent(text=str(result[8]) + " " + str(result[7]) + " " + str(result[3]), weight='bold',
                                  size='lg'),
                    # review
                    BoxComponent(
                        layout='baseline',
                        margin='md',
                        contents=[
                            IconComponent(size='xl',
                                          url='https://scontent.flhe9-1.fna.fbcdn.net/v/t1.0-9/64495893_2346660542061527_3073768691993673728_n.jpg?_nc_cat=110&_nc_ht=scontent.flhe9-1.fna&oh=4addf50a5f81c641eae11e5c55978a4c&oe=5D869A1F'),
                            TextComponent(text=str(result[1]) + ' THB/'+textList[0]+'   ', size='lg', color='#EA2225', margin='md',
                                          flex=0),


                        ]
                    ),
                    BoxComponent(
                        layout='baseline',
                        margin='md',
                        contents=[

                            IconComponent(size='sm',
                                          url='https://scontent.flhe9-1.fna.fbcdn.net/v/t1.0-9/61023711_296496927897333_8227976790065283072_n.png?_nc_cat=100&_nc_ht=scontent.flhe9-1.fna&oh=4b1dc23a99a79ab8b658a01e6efa0efd&oe=5D53F896'),

                            TextComponent(text=get_reviews+textList[1], size='sm', color='#666666', margin='md',
                                          flex=0)

                        ]
                    ),

                    # info
                    BoxComponent(
                        layout='vertical',
                        margin='lg',
                        spacing='sm',
                        contents=[
                            BoxComponent(
                                layout='baseline',
                                margin='md',
                                contents=[
                                    IconComponent(size='sm',
                                                  url='https://scontent.flhe9-1.fna.fbcdn.net/v/t1.0-9/62425269_2345087128885535_3594650042046087168_n.jpg?_nc_cat=110&_nc_oc=AQkUdWqQ0jLq5aqkxB8dpvHp5hvgMx08SQaGroJh0a5J851eBd4orttXJcVffUhK0SQ&_nc_ht=scontent.flhe9-1.fna&oh=47d4658e8e8f44c11d55fe681d06c020&oe=5D9D0983'),
                                    TextComponent(text=kms + ' '+textList[2]+'   ', size='sm', color='#666666', margin='sm',
                                                  flex=0),
                                    IconComponent(size='sm',
                                                  url='https://scontent.flhe9-1.fna.fbcdn.net/v/t1.0-9/60943613_296496961230663_5555595366230917120_n.png?_nc_cat=103&_nc_ht=scontent.flhe9-1.fna&oh=10344d8c9e01eab0235910f8ba386241&oe=5D96555F'),
                                    TextComponent(text=' ' + str(result[5]) + ' '+textList[3], size='sm', color='#666666',
                                                  margin='md',
                                                  flex=0)
                                ]
                            ),


                        ],
                    ),
                    BoxComponent(
                        layout='baseline',
                        margin='md',
                        contents=[
                            IconComponent(size='sm',
                                          url='https://scontent.flhe2-1.fna.fbcdn.net/v/t1.0-9/62477622_2345488378845410_6884576488062976000_n.jpg?_nc_cat=108&_nc_eui2=AeEkBOufG6ovKzDSRtBdGwdABI4KVSCvKWlDm_K6RTyQ1cPwUgxHaD1_3ZJQOvJVt7cbFVbKcCe3ewpeJWDHn7WJgC83xVxXWJCVSUHH9PdpE0eWLK1uemxAg1bZGbt_yGM&_nc_oc=AQmOC9EtEwUx-jvcLUxtFgeCudv5kZOGpJsLn0XT2O-QiBKqynCr6zETHCfa7NgLLsE&_nc_ht=scontent.flhe2-1.fna&oh=7b849506faf6f7989b8ab645a0e0231b&oe=5D921A18'),

                            TextComponent(str(result[11]).capitalize() + "         ", size='sm',
                                          color='#666666',
                                          margin='md',
                                          flex=0),
                            IconComponent(size='sm',
                                          url='https://scontent.flhe2-1.fna.fbcdn.net/v/t1.0-9/62447386_2345457338848514_2037429934288273408_n.jpg?_nc_cat=105&_nc_eui2=AeE5DiOIF0JlWG6vvZcWcmxBI7-bwinLKGxIbjcgy-BPzoAZTHXWoGi6ppkA0ljbbopOYkFAlizswv3evuTnEWOovN4_NYwdzXsRiAYMQfAcHheR9OdxXaeU9LpCmoA0v7k&_nc_oc=AQl5y-ocnxMXsRBinFIIACetdjNzQzhqNgguC9_nfBRyvh8dRtuhJbb1zODPkU7DzIA&_nc_ht=scontent.flhe2-1.fna&oh=644e85134892201afcb62c2ab0590aa6&oe=5D8F3663'),
                            TextComponent(text=str(result[9]) + '  '+textList[5], size='sm', color='#666666',
                                          margin='md',
                                          flex=0),

                        ]
                    ),
                    BoxComponent(
                        layout='baseline',
                        margin='md',
                        contents=[
                            TextComponent(text=" "+textList[6]+": " + str(result[10]).capitalize() + " ",
                                          size='sm', color='#666666',
                                          margin='md',
                                          flex=0),
                            TextComponent(text=payment,
                                          size='sm', color='#666666',
                                          margin='md',
                                          flex=0)
                        ]
                    ),
                ],
            ),
            footer=BoxComponent(
                layout='horizontal',
                spacing='sm',
                contents=[
                    # callAction, separator, websi
                    # SpacerComponent(size='sm'),
                    # callAction
                    ButtonComponent(
                        style='primary',
                        height='sm',
                        color='#EA2225',
                        # action=MessageAction(label='Book Now', text='Book Now'),
                        action=PostbackAction(label=textList[9], data='Book Car Id ' + str(id), display_text=textList[9])
                    ),
                    # separator
                    SeparatorComponent(size='sm'),
                    # websiteAction
                    ButtonComponent(
                        style='primary',
                        height='sm',
                        color='#666666',
                        action=PostbackAction(label=textList[10],
                                              data='View Detail Id ' + str(id) + "-" + str(distance),
                                              display_text=textList[10])

                    )
                ]
            ),
        )

    return bubble

def carCardList(languageCode):
    if languageCode=='th':
        textList=['วัน','reviews','ระยะทาง','ที่นั่ง','Automatic','กิโลเมตร','ชนิดเชื้อเพลิง','บัตรเครดิตเท่านั้น','เงินสด / บัตรเครดิต','จองตอนนี้','ภาพถ่าย']
    else:
        textList=['Day','reviews','Km Away','Seats','Automatic','Km','Fuel Type','Credit Card Only','Cash/Credit Card','BOOK NOW','PHOTOS']
    return textList



def cardDetailTexts(lang):
    if lang=='th':
        textList=["ย้อนกลับ"]
    else:
        textList=["Go Back"]
    return textList
#This function returns of the detail of the car (More pictures)
def CarCardDetail(id, distance,event):
    lang=getLangCode(event)
    textList=cardDetailTexts(lang)
    kms = str(distance)
    kms = kms[0:4]
    # import mysql.connector
    cnx = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
    cursor = cnx.cursor()
    get_reviews = getReviews(id)
    cols_data=[]
    q = "select photo,id from car_cargallery where car_id='%s' order by id desc LIMIT 9" % (id)
    cursor.execute(q)
    r = cursor.fetchall()
    if cursor.rowcount > 0:
        for cars in r:
            #New Changes
            img= ImageCarouselColumn(image_url="https://www.drivemate.asia/media/" + str(cars[0]),
                                      action=PostbackAction(label=textList[0], data='Go Back ', display_text=textList[0])
                                     )

            cols_data.append(img)

    
        return cols_data,1
    else:
        return cols_data,0

def brandNamesTexts(languageCode):
    if languageCode=='th':
        textList=['โปรดระบุการตั้งค่าแบรนด์ของคุณหากมี','BMW','Mercedece Benz','Audi','Honda','Toyota','Mazda','Ford','None']
    else:
        textList=['Please specify your brand preference if any.','BMW','Mercedece Benz','Audi','Honda','Toyota','Mazda','Ford','None']
    return textList

# This function returns the brand names quick replies.
def brand_names(languageCode):
    textList=brandNamesTexts(languageCode)
    message = TextSendMessage(
        text=textList[0],
        quick_reply=QuickReply(
            items=[

                QuickReplyButton(
                    image_url="https://scontent.flhe9-1.fna.fbcdn.net/v/t1.0-9/24129880_10155853725431303_1906346036257260983_n.jpg?_nc_cat=1&_nc_ht=scontent.flhe9-1.fna&oh=edeb6850006618ca568552636ec344e5&oe=5D358E81",
                    action=MessageAction(label="BMW", text="BMW")
                ),
                QuickReplyButton(
                    image_url="https://scontent.flhe9-1.fna.fbcdn.net/v/t1.0-9/933917_10151766494511670_1984178194_n.jpg?_nc_cat=1&_nc_ht=scontent.flhe9-1.fna&oh=e60f5761031547a049a5d06a37924fea&oe=5D71C33F",
                    action=MessageAction(label="Mercedece Benz", text="Mercedece Benz")
                ),
                QuickReplyButton(
                    image_url="https://scontent.flhe6-1.fna.fbcdn.net/v/t1.0-9/13087455_622675491217458_8547609933039052818_n.png?_nc_cat=107&_nc_eui2=AeEL51X0docbH34gYC1huhGeWWytGHOmWtw_lGVUj12TTOgkmFnEoCTLTrSnuDW-FfS7cLpREOgaFPrPfGbaejok9k8ZGQRNHTonCitWgGIRBg&_nc_ht=scontent.flhe6-1.fna&oh=b4a03264e7adde2894bb24529d798c49&oe=5D9BB108",
                    action=MessageAction(label="Audi", text="Audi")
                ),
                QuickReplyButton(
                    image_url="https://scontent.flhe9-1.fna.fbcdn.net/v/t1.0-9/10696307_10152706850109275_1708986217665329844_n.jpg?_nc_cat=1&_nc_ht=scontent.flhe9-1.fna&oh=1bdfd67f865282fac77c8d4cfd867707&oe=5D3561E1",
                    action=MessageAction(label="Honda", text="Honda")
                ),
                QuickReplyButton(
                    image_url="https://scontent.flhe9-1.fna.fbcdn.net/v/t1.0-9/1233559_670206019657491_1994268078_n.jpg?_nc_cat=107&_nc_ht=scontent.flhe9-1.fna&oh=f0a7b26e6686524d9bf6284cea66b468&oe=5D2DF590",
                    action=MessageAction(label="Toyota", text="Toyota")
                ),
                QuickReplyButton(
                    image_url="https://scontent.flhe6-1.fna.fbcdn.net/v/t1.0-9/33468444_10160315769170363_1397762914248556544_n.jpg?_nc_cat=1&_nc_eui2=AeHdiuK89hAByt1-R09ZLBd03gFcjCOU_s3cJE5tHPONsw_9sm_3girP0ASnIPAv2MJyFfacVhB72Tei1dOKhXgCTfy3ZAfx3ENhqFYg8wb8cA&_nc_ht=scontent.flhe6-1.fna&oh=ea6784c7bdc0fae7748a397ae7a995ca&oe=5D9819F1",
                    action=MessageAction(label="Mazda", text="Mazda")
                ),
                QuickReplyButton(
                    image_url="https://scontent.flhe2-1.fna.fbcdn.net/v/t1.0-9/17190746_10154912060270049_4778172697714499766_n.jpg?_nc_cat=1&_nc_eui2=AeGVRLI4SVSQ8YAsTGyJyOPEmv4SbRk_--qCxyilsfQj5JyOZ_m4CDqGTrqqSICQtyKrMDQpOtJfhob_ZtJYOx32fRuxVbA-OSAgOUANAGHLPwC3tJeUef3WwoztbjpXjqI&_nc_ht=scontent.flhe2-1.fna&oh=8c62c82a1ddbb727987115c0e0a94738&oe=5D95905E",
                    action=MessageAction(label="Ford", text="Ford")
                ),
                QuickReplyButton(
                    image_url="https://cdn.onlinewebfonts.com/svg/img_249401.png",
                    action=MessageAction(label="None", text="None")
                )
            ]))
    return message

def pricesTexts(languageCode):
    if languageCode=='th':
        textList=['โปรดระบุช่วงราคาที่คุณต้องการ','1-1500','1500-3000','3000-6000','6000-12000','12000-24000','24000-48000','48000-96000','96000-Above','None']
    else:
        textList=['Please specify your price range preference.','1-1500','1500-3000','3000-6000','6000-12000','12000-24000','24000-48000','48000-96000','96000-Above','None']
    return textList


#This function returns the prices quick replies.
def prices(languageCode):

    textList=pricesTexts(languageCode)
    message = TextSendMessage(
        text=textList[0],
        quick_reply=QuickReply(
            items=[

                QuickReplyButton(
                    action=MessageAction(label="1-1500", text="1-1500")
                ),
                QuickReplyButton(

                    action=MessageAction(label="1500-3000", text="1500-3000")
                ),
                QuickReplyButton(

                    action=MessageAction(label="3000-6000", text="3000-6000")
                ),
                QuickReplyButton(

                    action=MessageAction(label="6000-12000", text="6000-12000")
                ),
                QuickReplyButton(

                    action=MessageAction(label="12000-24000", text="12000-24000")
                ),
                QuickReplyButton(

                    action=MessageAction(label="24000-48000", text="24000-48000")
                ),
                QuickReplyButton(

                    action=MessageAction(label="48000-96000", text="48000-96000")
                ),
                QuickReplyButton(

                    action=MessageAction(label="96000-Beyond", text="96000-Beyond")
                ),
                QuickReplyButton(

                    action=MessageAction(label="None", text="0-10000")
                )
            ]))
    return message

#This function takes a text inpur and returns its latitude and lognitude
def geocode_me(Location):
    try:
        return geolocator.geocode(Location)
    except GeocoderTimedOut:
        return geocode_me(Location)

# This function handles the convertion of text into lat long for city names.
def handleConversion(event):
    text = event.message.text

    location = ['BANGKOK', 'CHON BURI', 'CHIANG MAI', 'CHIANG RAI', 'LAMPANG', 'KRABI', 'PHUKET',
                'SING BURI','SURAT THANI','กรุงเทพมหานคร','ชลบุรี','เชียงใหม่','เชียงราย','ลำปาง','กระบี่','ภูเก็ต','สิงห์บุรี','สุราษฎร์ธานี']
    if text.upper() in location:

        try:
            location = geocode_me(text)
            lang=getLangCode(event)
            if lang=='th':
                print("****LOCATION")
                print(location)
                return  'ที่ตั้ง ' + 'ละติจูด ' + str(location.latitude) + ' ลองจิจูด ' + str(location.longitude)
            else:
                print("****LOCATION")
                print(location)
                return 'location lat ' + str(location.latitude) + ' long ' + str(location.longitude)
        except:
            print("Geocoder failed")
    else:
        return text


# This function returns the reviews of each car takes input as car id.
def getReviews(id):
    cnx = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')

    cursor = cnx.cursor()

    review = "SELECT AVG(rating) as rating, count(car_id) as number FROM `review_review` WHERE car_id='%s' and publish=1 and type='car'" % (
        id)
    cursor.execute(review)
    rev = cursor.fetchall()
    reviews = reviews = '☆' * 5 + " " + str(0)
    for r in rev:
        review_count = (r[1])
        if review_count > 0:
            stars = round(r[0])
            reviews = '⭐' * stars + '☆' * (5 - stars) + " " + str(review_count) + "  "

        elif review_count == 0:
            reviews = '☆' * 5 + " " + str(0) + "  "

    return reviews

# This function displays the next 9 cars when a postback event is received.
def showMore(event):
    print("Showing More Cars")
    try:
        input_data = event.postback.data
        prev = 'Show Previous Cars Id '
        next = 'Show Next Cars Id '
        if prev in input_data:
            word_list = input_data.split()
            min = word_list[-1].split('-')[0]
            max = word_list[-1].split('-')[1]
            print("FETCHING INDEX")
            print(word_list[-1])
            showP(int(min), int(max), event)

        elif next in input_data:
            word_list = input_data.split()
            print("FETCHING INDEX")
            print(word_list[-1])
            min = word_list[-1].split('-')[0]
            max = word_list[-1].split('-')[1]
            print(min)
            print(max)
            # word_list[-1]
            showN(int(min), int(max), event)

    except:
        print("There are no more cars available right now.")

# This function displays the next 9 cars when a postback event is received.
def showN(min, max, event):
    try:
        lang=getLangCode(event)
        print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        print(lang)
        min_ind = min
        max_ind = max
        user_id = event.source.user_id
        newconnection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
        cursor = newconnection.cursor()
        data_array = []
        list_of_flex = []
        rowcount = "select max(row_count) from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q " % (
            user_id)
        cursor.execute(rowcount)
        r = cursor.fetchone()
        print("************** SHOWING ROW COUNT")
        print(r)
        print(r[0])
        if r[0] > max:
            min_ind = max + 1
            # q="select car_id,distance from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q where row_count >'%s' '"%(user_id,max)
            q = "select car_id,distance,row_count from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q where row_count >'%s' and row_count <='%s'" % (
                user_id, max, max + 9)
        else:
            q = "select car_id,distance,row_count from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q where row_count >='%s' and row_count <='%s'" % (
                user_id, min, max)
        cursor.execute(q)
        res = cursor.fetchall()
        if cursor.rowcount > 0:
            for result in res:
                print("Printing Result CAR ID DISTANCE ROW NUMBER")
                print(result[0])
                print(result[1])
                print("Printing Result MIN ROW COUNT")
                print(result[2])

                data_array.append(CarCard(result[0], result[1],lang))
                print(result[0])
                print("carID")
                print(result[1])
                print("distance")
                max_ind = result[2]
            for i in range(cursor.rowcount):
                list_of_flex.append(data_array[i])
            my_message = CarouselContainer(contents=list_of_flex)
            message = FlexSendMessage(alt_text="Search Result", contents=my_message)
            control_message = show_controls(r[0], min_ind, max_ind,event)
            message_array = [message, control_message]
            run.line_bot_api.reply_message(
                event.reply_token,
                message_array
            )
            # update search history with min max
            updateSearchHistory(event, min_ind, max_ind)

    except mysql.connector.Error as error:
        newconnection.rollback()
        print("Failed to fetch more cars {}".format(error))
    finally:
        if (newconnection.is_connected()):
            cursor.close()
            newconnection.close()
            print("MySQL connection is closed")

# This function displays the previous 9 cars when a postback event is received.
def showP(min, max, event):
    try:
        lang=getLangCode(event)
        user_id = event.source.user_id
        newconnection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
        cursor = newconnection.cursor()
        data_array = []
        list_of_flex = []
        rowcount = "select max(row_count) from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q " % (
            user_id)
        cursor.execute(rowcount)
        r = cursor.fetchone()
        print("************** SHOWING ROW COUNT")
        print(r)
        print(r[0])

        if max > 9:
            max_ind = min - 1
            min_ind = min - 9
            # q="select car_id,distance from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q where row_count >'%s' '"%(user_id,max)
            q = "select car_id,distance,row_count from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q where row_count >='%s' and row_count <='%s'" % (
                user_id, min_ind, max_ind)
            cursor.execute(q)
            res = cursor.fetchall()
            if cursor.rowcount > 0:
                for result in res:
                    print("Printing Result CAR ID DISTANCE ROW NUMBER")
                    print(result[0])
                    print(result[1])
                    print("Printing Result MIN ROW COUNT")
                    print(result[2])
                    data_array.append(CarCard(result[0], result[1],lang))
                    print(result[0])
                    print("carID")
                    print(result[1])
                    print("distance")

                for i in range(cursor.rowcount):
                    list_of_flex.append(data_array[i])
                my_message = CarouselContainer(contents=list_of_flex)
                message = FlexSendMessage(alt_text="Search Result", contents=my_message)
                control_message = show_controls(r[0], min_ind, max_ind,event)
                message_array = [message, control_message]
                run.line_bot_api.reply_message(
                    event.reply_token,
                    message_array
                )
                # UPDATE search History
                updateSearchHistory(event, min_ind, max_ind)

        else:
            min_ind = min
            max_ind = max
            q = "select car_id,distance,row_count from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q where row_count >='%s' and row_count <='%s'" % (
                user_id, min_ind, max_ind)
            cursor.execute(q)
            res = cursor.fetchall()
            if cursor.rowcount > 0:
                for result in res:
                    print("Printing Result CAR ID DISTANCE ROW NUMBER")
                    print(result[0])
                    print(result[1])
                    print("Printing Result MIN ROW COUNT")
                    print(result[2])
                    data_array.append(CarCard(result[0], result[1],lang))
                    print(result[0])
                    print("carID")
                    print(result[1])
                    print("distance")
                    max_ind = result[2]
                for i in range(cursor.rowcount):
                    list_of_flex.append(data_array[i])
                my_message = CarouselContainer(contents=list_of_flex)
                message = FlexSendMessage(alt_text="Search Result", contents=my_message)
                control_message = show_controls(r[0], min_ind, max_ind,event)
                message_array = [message, control_message]
                run.line_bot_api.reply_message(
                    event.reply_token,
                    message_array
                )
                # UPDATE SEARCH HISTORY
                updateSearchHistory(event, min_ind, max_ind)
    except mysql.connector.Error as error:
        newconnection.rollback()
        print("Failed to fetch more cars {}".format(error))
    finally:
        if (newconnection.is_connected()):
            cursor.close()
            newconnection.close()
            print("MySQL connection is closed")

#This function updates the user search history when he clicks next or previous
def updateSearchHistory(event, min, max):
    try:
        newconnection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
        cursor = newconnection.cursor()
        q = "select max(id) from chatbot_searchhistory where user_id='%s' " % (event.source.user_id)
        cursor.execute(q)
        r = cursor.fetchone()
        sql_update_query = """Update chatbot_searchhistory set from_ind = %s,to_ind=%s where id = %s"""
        input = (min, max, r[0])
        cursor.execute(sql_update_query, input)
        newconnection.commit()
        print("Record inserted successfully into python_users table")
    except mysql.connector.Error as error:
        newconnection.rollback()
        print("Failed to insert into MySQL table {}".format(error))
    finally:
        if (newconnection.is_connected()):
            cursor.close()
            newconnection.close()
            print("MySQL connection is closed")

# When a user clicks on view car detail buttons this fucntion handles the event and displays the details.
def viewCarDetail(event):
    data = event.postback.data
    word_list = data.split()
    print("FETCHING View CAR Detail INDEX")
    print(word_list[-1])
    carId = word_list[-1].split('-')[0]
    distance = word_list[-1].split('-')[1]
    print(carId)
    print(distance)
    bubbles_array,status = CarCardDetail(carId, distance,event)
    if status==1:
        image_carousel_template = ImageCarouselTemplate(columns=bubbles_array)
        
        template_message = TemplateSendMessage(
            alt_text='ImageCarousel alt text', template=image_carousel_template)
        run.line_bot_api.reply_message(event.reply_token, template_message)
    else:
        pass
    # array = CarouselContainer(contents=bubbles_array)
    # message = FlexSendMessage(alt_text="Search Result", contents=array)
    #
    # run.line_bot_api.reply_message(
    #     event.reply_token,
    #     message
    # )


#This function logs the cars search history into the database which is used later to go back to the search preference.
def showCarsHistory(event):
    try:
        data_array = []
        list_of_flex = []
        min = 1
        max = 9
        newconnection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
        cursor = newconnection.cursor()
        q = "select max(id),from_ind,to_ind from chatbot_searchhistory where user_id='%s' " % (event.source.user_id)
        q = "select id,from_ind,to_ind from (select * from chatbot_searchhistory where created_at= (select max(created_at) from chatbot_searchhistory where user_id='%s' group by user_id))q " % (
            event.source.user_id)

        cursor.execute(q)
        r = cursor.fetchone()
        min = r[1]
        max = r[2]
        # for res in r:
        #     min=r[1]
        #     max=r[2]

        print("GOT THE INDEXES")
        print(min)
        print(max)

        # q="select car_id,distance from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q where row_count >'%s' '"%(user_id,max)
        q = "select car_id,distance,row_count from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q where row_count >='%s' and row_count <='%s'" % (
            event.source.user_id, min, max)
        cursor.execute(q)
        res = cursor.fetchall()
        if cursor.rowcount > 0:
            for result in res:
                print("Printing Result CAR ID DISTANCE ROW NUMBER")
                print(result[0])
                print(result[1])
                print("Printing Result MIN ROW COUNT")
                print(result[2])
                lang=getLangCode( event)
                data_array.append(CarCard(result[0], result[1],lang))
                print(result[0])
                print("carID")
                print(result[1])
                print("distance")

            for i in range(cursor.rowcount):
                list_of_flex.append(data_array[i])
            my_message = CarouselContainer(contents=list_of_flex)
            message = FlexSendMessage(alt_text="Search Result", contents=my_message)
            control_message = show_controls(r[0], min, max,event)
            message_array = [message, control_message]
            run.line_bot_api.reply_message(
                event.reply_token,
                message_array
            )



    except mysql.connector.Error as error:
        newconnection.rollback()
        print("Failed to insert into MySQL table {}".format(error))
    finally:
        if newconnection.is_connected():
            cursor.close()
            newconnection.close()
            print("MySQL connection is closed")

def getLangCode(event):
    try:
        newconnection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
        cursor = newconnection.cursor()
        q="select chatbot_messages.id,chatbot_messages.language_code from chatbot_messages where chatbot_messages.id in (select max(chatbot_messages.id) from chatbot_messages inner join chatbot_users on chatbot_users.id=chatbot_messages.from_user where chatbot_users.line_id='%s')"%(event.source.user_id)       
        cursor.execute(q)
        r = cursor.fetchone()


        return r[1]

        # q="select car_id,distance from (select * from chatbot_usersession where requesttime= (select max(requesttime) from chatbot_usersession where user_id='%s' group by user_id))q where row_count >'%s' '"%(user_id,max)



    except mysql.connector.Error as error:
        newconnection.rollback()
        print("Failed to insert into MySQL table {}".format(error))
        return 'th'
    finally:
        if newconnection.is_connected():
            cursor.close()
            newconnection.close()
            print("MySQL connection is closed")
