#coding=utf-8
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode
import responses
import time
import datetime
from datetime import timedelta

#this function takes the prices from input and sets the bucket accordingly
def getrange(price):
    try:
        if price >= 1 and price <=1500:
            return [1, 1500]
        elif price >= 1500 and price <=3000:
            return [1500, 3000]
        elif price >= 3000 and price <=6000:
            return [3000, 6000]
        elif price >= 6000 and price <=12000:
            return [6000, 12000]
        elif price >= 12000 and price <= 24000:
            return [12000, 24000]
        elif price >= 24000 and price <=48000:
            return [24000, 48000]
        elif price >= 48000 and price <=96000:
            return [48000, 96000]
        elif price >= 96000:
            return [96000, 150000]
        elif price == 0:
            return [1, 150000]

    except:
        return [1, 150000]

# This function gets the brand name and returns the brand_ids for those brands.
def getbrands(brand_name):
    brands_list = []
    print("*********************** BRAND NAME **********")
    print(brand_name)
    brand_name = str(brand_name)
    try:
        newconnection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
        cursor = newconnection.cursor()

        if brand_name != 'None':
            q = "select id from master_car_carbrand where name='%s'" % (brand_name)
            cursor.execute(q)
            res = cursor.fetchall()
            for result in res:
                brands_list.append(int(result[0]))

        elif brand_name == 'None':
            q = "select id from master_car_carbrand where 1"
            cursor.execute(q)
            res = cursor.fetchall()
            for result in res:
                brands_list.append(int(result[0]))

        print("BRANDS **************************************")
        print(brands_list)
        return brands_list

    except mysql.connector.Error as error:
        # newconnection.rollback()
        return [0]
        print("Failed to fetch brand ids from MySQL table {}".format(error))
    finally:
        if (newconnection.is_connected()):
            cursor.close()
            newconnection.close()
            print("MySQL connection is closed")

#This function checks if a car brand is in the list of brands.
def checkBrand(caBrandId, brands_list):
    if caBrandId in brands_list:
        return 1
    else:
        return 0

# This function handles the quick search by location by taking input from dialogflow. Trnasform the input and calling sql query to fetch data from database.
def searchCarsbyLocation(response, event):
    latitude_vr = 13.863
    longitude_vr = 100.5018
    start_date = '2019-05-13 00:00:00'
    end_date = '2019-05-30 00:00:00'
    area = 20
    driver = 0
    if 'parameters' in response['queryResult']:
        if 'rent-type' in response['queryResult']['parameters']:
            rent_type = str(response['queryResult']['parameters']['rent-type'])
            if rent_type == 'Rental with driver':
                driver = 1
            elif rent_type == 'Rental':
                driver = 0
            elif rent_type== 'Monthly Rental':
                driver=0
        if 'lat' in response['queryResult']['parameters']:
            latitude_vr = str(response['queryResult']['parameters']['lat'])
        if 'long' in response['queryResult']['parameters']:
            longitude_vr = str(response['queryResult']['parameters']['long'])
        if 'start-time' in response['queryResult']['parameters']:
            _start_date = '20' + str(int(response['queryResult']['parameters']['start-time']))
        if 'end-time' in response['queryResult']['parameters']:
            _end_date = '20' + str(int(response['queryResult']['parameters']['end-time']))

    start_date = _start_date[0:4] + "-" + _start_date[4:6] + "-" + _start_date[6:8] + " " + _start_date[8:10] + ":" + _start_date[10:12] + ":" + "00"
    end_date = _end_date[0:4] + "-" + _end_date[4:6] + "-" + _end_date[6:8] + " " + _end_date[8:10] + ":" + _end_date[10:12] + ":" + "00"

    print("Printing Received variables:")
    print(rent_type, driver, latitude_vr, longitude_vr, start_date, end_date)

    # saveSearchPreference(rent_type,latitude_vr,longitude_vr,start_date,end_date,0,'None',event)

    cnx = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
    cursor = cnx.cursor()
    cursor_ = cnx.cursor()
    # Calculate Arial Distance
    query = "Select * from (SELECT id,has_driver,price_per_day,latitude,longitude,location,city,(((acos(sin(('%s'*pi()/180)) * sin((latitude*pi()/180))+cos(('%s'*pi()/180)) * cos((Latitude*pi()/180)) * cos((('%s' - longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344) as distance FROM car_car) Q where distance<='%s' and has_driver= '%s' and price_per_day  > 0 order by distance" % (
    latitude_vr, latitude_vr, longitude_vr, area, driver)
    cursor.execute(query)
    rv = cursor.fetchall()
    cars_count = cursor.rowcount
    print("**********************PRINT CARS COUNT")
    print(cars_count)
    data_array = []  # data array would be a session variable
    count = 0
    num = 0
    req_t = time.time()
    req_time = datetime.datetime.fromtimestamp(req_t).strftime('%Y-%m-%d %H:%M:%S')

    for result in rv:
        # Check the availibility for the cars in that area
        subquery = " SELECT b.car_id,start,end FROM (SELECT car_id, pickup_at as start, return_at as end from `order_order` WHERE car_id='%s' AND status='complete' union select car_id, start_at as start, end_at as end from car_cardisableddate WHERE car_id='%s') b WHERE NOT (b.end <= CAST('%s' AS DATETIME) OR b.start >= CAST('%s' AS DATETIME)) " % (
            result[0], result[0], start_date, end_date)
        cursor_.execute(subquery)
        rs = cursor_.fetchall()
        if cursor_.rowcount <= 0:
            # if cursor_.rowcount == -1:
            count = count + 1
            if count <= 9:
                num = num + 1
                data_array.append(responses.CarCard(result[0], result[7],response['queryResult']['languageCode']))
            addToSession(event, count, req_time, result[0], result[7])
    saveSearchPreference(rent_type, latitude_vr, longitude_vr, start_date, end_date, 000, 'NULL', event, 1, num)

    return [data_array, count]

# This function handles the search with preference by taking input from dialogflow. Trnasform the input and calling sql query to fetch data from database.
def searchWithPreference(response, event):
    languageCode=responses.getLangCode(event)
    latitude_vr = 13.863
    longitude_vr = 100.5018
    start_date = '2019-05-13 00:00:00'
    end_date = '2019-05-30 00:00:00'
    area = 40
    driver = 0
    price = 0
    brand_name = 'None'
    if 'parameters' in response['queryResult']:
        if 'rent-type' in response['queryResult']['parameters']:
            rent_type = str(response['queryResult']['parameters']['rent-type'])
            if rent_type == 'Rental with driver':
                driver = 1
            elif rent_type == 'Rental':
                driver = 0
        if 'lat' in response['queryResult']['parameters']:
            latitude_vr = str(response['queryResult']['parameters']['lat'])
        if 'long' in response['queryResult']['parameters']:
            longitude_vr = str(response['queryResult']['parameters']['long'])
        if 'start-time' in response['queryResult']['parameters']:
            _start_date = '20' + str(int(response['queryResult']['parameters']['start-time']))
        if 'end-time' in response['queryResult']['parameters']:
            _end_date = '20' + str(int(response['queryResult']['parameters']['end-time']))
        if 'price' in response['queryResult']['parameters']:
            price = response['queryResult']['parameters']['price']
        if 'brand-name' in response['queryResult']['parameters']:
            brand_name = response['queryResult']['parameters']['brand-name']

    start_date = _start_date[0:4] + "-" + _start_date[4:6] + "-" + _start_date[6:8] + " " + _start_date[8:10] + ":" + _start_date[10:12] + ":" + "00"
    end_date = _end_date[0:4] + "-" + _end_date[4:6] + "-" + _end_date[6:8] + " " + _end_date[8:10] + ":" + _end_date[10:12] + ":" + "00"
    price_range = getrange(price)
    print("*********PRICE RANGE")
    print(price_range)
    print("***********BRANDSIDS")
    brand_ids = getbrands(brand_name)
    print(brand_ids)
    print("Printing Received variables:")
    print(rent_type, driver, latitude_vr, longitude_vr, start_date, end_date, price, brand_name)

    # saveSearchPreference(rent_type,latitude_vr,longitude_vr,start_date,end_date,price,brand_name,event)
    cnx = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
    cursor = cnx.cursor()
    cursor_ = cnx.cursor()
    # Calculate Arial Distance
    query = "Select * from (SELECT id,has_driver,price_per_day,latitude,longitude,location,city,brand_id,(((acos(sin(('%s'*pi()/180)) * sin((latitude*pi()/180))+cos(('%s'*pi()/180)) * cos((Latitude*pi()/180)) * cos((('%s' - longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344) as distance FROM car_car) Q where distance<='%s' and has_driver= '%s' and price_per_day  >= '%s' and price_per_day  <= '%s' order by distance,price_per_day" % (
    latitude_vr, latitude_vr, longitude_vr, area, driver, price_range[0], price_range[1])
    cursor.execute(query)
    rv = cursor.fetchall()
    cars_count = cursor.rowcount
    print("**********************PRINT CARS COUNT")
    print(cars_count)

    data_array = []  # data array would be a session variable
    count = 0
    num = 0
    req_t = time.time()
    req_time = datetime.datetime.fromtimestamp(req_t).strftime('%Y-%m-%d %H:%M:%S')

    for result in rv:
        # Check the availibility for the cars in that area
        subquery = " SELECT b.car_id,start,end FROM (SELECT car_id, pickup_at as start, return_at as end from `order_order` WHERE car_id='%s' AND status='complete' union select car_id, start_at as start, end_at as end from car_cardisableddate WHERE car_id='%s') b WHERE NOT (b.end <= CAST('%s' AS DATETIME) OR b.start >= CAST('%s' AS DATETIME)) " % (
            result[0], result[0], start_date, end_date)
        cursor_.execute(subquery)
        rs = cursor_.fetchall()
        if cursor_.rowcount <= 0:
            print(result[7])
            if checkBrand(result[7], brand_ids):
                count = count + 1
                if count <= 9:
                    num = num + 1
                    data_array.append(responses.CarCard(result[0], result[8],languageCode))
                addToSession(event, count, req_time, result[0], result[8])

    saveSearchPreference(rent_type, latitude_vr, longitude_vr, start_date, end_date, price, brand_name, event, 1, num)
    return [data_array, count]

# This function saves the user's search preference into database for later reference.
def saveSearchPreference(rent_type, latitude_vr, longitude_vr, _start_date, _end_date, price, brand_name, event,
                         from_ind, to_ind):
    try:

        connection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
        #cursor = connection.cursor(prepared=True)
        cursor = connection.cursor()
        sql_insert_query = """ INSERT INTO `chatbot_searchhistory`
                            ( `user_id`, `rent_type`, `latitude`, `longitude`, `start_date`, `end_date`, `price_range`, `brand`,`from_ind`, `to_ind`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        insert_tuple = (
        event.source.user_id, rent_type, latitude_vr, longitude_vr, _start_date, _end_date, price, brand_name, from_ind,
        to_ind)
        result = cursor.execute(sql_insert_query, insert_tuple)
        connection.commit()
        print("Record inserted successfully into python_users table")
    except mysql.connector.Error as error:
        connection.rollback()
        print("Failed to insert into MySQL table {}".format(error))
    finally:
        # closing database connection.
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")

# This tables populates the usersession table with his search preference.
def addToSession(event, row_number, request_time, carId, distance):
    try:
        connection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
        #cursor = connection.cursor(prepared=True)
        cursor=connection.cursor()
        sql_insert_query = """ INSERT INTO `chatbot_usersession`
                            (`user_id`, `row_count`, `car_id`, `distance`,`requesttime` ) VALUES (%s,%s,%s,%s,%s)"""
        insert_tuple = (event.source.user_id, row_number, carId, distance, request_time)
        result = cursor.execute(sql_insert_query, insert_tuple)
        connection.commit()
        print("Record inserted successfully into python_users table")
    except mysql.connector.Error as error:
        connection.rollback()
        print("Failed to insert into MySQL table {}".format(error))
    finally:
        #print("Close the cursor.")
        # closing database connection.
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")





