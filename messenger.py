#coding=utf-8
import run
import mysql.connector
import time
import datetime
from datetime import timedelta
from mysql.connector import Error
from mysql.connector import errorcode


def log(event, text, response):
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    registeruser(event, timestamp)
    logusermessage(event, text, timestamp,response)
    logresponse(event, response, timestamp)


def registeruser(event, timestamp):
    userId=event.source.user_id
    profile = run.line_bot_api.get_profile(userId)
    user_name = profile.display_name
    image = 'https://via.placeholder.com/150x150'
    try:
        image = profile.picture_url
    except:
        image = 'https://via.placeholder.com/150x150'

    # try:
    connection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
    #cursor = connection.cursor(prepared=True)
    cursor = connection.cursor()
    q = "select * from chatbot_users where line_id='%s'" % (userId)
    cursor.execute(q)
    res = cursor.fetchone()
    print("TRYING TO REGISTER USERS")
    print(cursor.rowcount)
    # print(cursor.rowcount)
    if cursor.rowcount <= 0:
        sql_insert_query = """ INSERT INTO `chatbot_users`
                            (`line_id`, `name`, `email`, `password`, `profile_image`, `created_at`, `updated_at`) VALUES (%s,%s,%s,%s,%s,%s,%s)"""
        insert_tuple = (userId, user_name, userId + '@line.com', 'secret', image, timestamp, timestamp)
        result = cursor.execute(sql_insert_query, insert_tuple)
        connection.commit()
        print("Record inserted successfully into python_users table")


def logusermessage(event, text, timestamp,response):
    
    try:
        languageCode=response["queryResult"]["languageCode"]
        print("***************************")
        print(languageCode)
        userId=event.source.user_id
        print("*******TIMESTAMP")
        print(timestamp)
        connection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
        #cursor = connection.cursor(prepared=True)
        cursor = connection.cursor()
        q = "select id from chatbot_users where line_id='%s'" % (userId)
        cursor.execute(q)
        res = cursor.fetchone()
        if cursor.rowcount > 0:
            sql_insert_query = """ INSERT INTO `chatbot_messages`
                                (`from_user`, `to_user`, `read`, `text`,`language_code`,`created_at`,`updated_at`) VALUES (%s,%s,%s,%s,%s,%s,%s)"""
            insert_tuple = (res[0], 3, 0, text, languageCode ,timestamp, timestamp)
            result = cursor.execute(sql_insert_query, insert_tuple)
            connection.commit()
            print("Record inserted successfully into python_users table")
    except mysql.connector.Error as error:
        connection.rollback()
        print("******************* IN MESSAGE LOGGER")
        print("Failed to insert into MySQL table {}".format(error))
    finally:
        # closing database connection.
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


def logresponse(event, response, timestamp):
    
    try:
        userId=event.source.user_id
        resp = "fulfillmentText"
        if 'fulfillmentText' in response["queryResult"]:
            #resp = response["queryResult"]['fulfillmentText']
            resp= response["queryResult"]['fulfillmentText']
        else:
            resp='fullfillmentText '
        # print(response)
        newconnection = mysql.connector.connect(host='128.199.84.245',
                                         database='drivemate_staging',
                                         user='chatbot',
                                         password='1234')
        #cursor = newconnection.cursor(prepared=True)
        cursor = newconnection.cursor()
        q = "select id from chatbot_users where line_id='%s'" % (userId)
        cursor.execute(q)
        res = cursor.fetchone()
        if cursor.rowcount > 0:
            sql_insert_query = """ INSERT INTO `chatbot_messages`
                                (`from_user`, `to_user`, `read`, `text`, `language_code`,`created_at`,`updated_at`) VALUES (%s,%s,%s,%s,%s,%s,%s)"""
            insert_tuple = (3, res[0], 0, resp , response['queryResult']['languageCode'],timestamp, timestamp)
            result = cursor.execute(sql_insert_query, insert_tuple)
            newconnection.commit()
            print("Record inserted successfully into python_users table")
    except mysql.connector.Error as error:
        newconnection.rollback()
        print("************** IN RESPONSE LOGGER")
        print("Failed to insert into MySQL table {}".format(error))
    finally:
        if (newconnection.is_connected()):
            cursor.close()
            newconnection.close()
            print("MySQL connection is closed")
