#coding=utf-8
from __future__ import unicode_literals
import uuid
import errno
import os
import sys
import tempfile
import responses
import queries
import messenger
import random
from google.protobuf.json_format import MessageToJson
import json
from argparse import ArgumentParser
from flask import Flask, request, abort, send_from_directory, jsonify, session
from google.protobuf.json_format import MessageToJson
import google.protobuf  as pf
# import simplejson as json
from geopy.geocoders import Nominatim
from langdetect import detect
import argparse
import uuid

geolocator = Nominatim(user_agent="Python")
# from werkzeug.middleware.proxy_fix import ProxyFix

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    LineBotApiError, InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
    SourceUser, SourceGroup, SourceRoom,
    TemplateSendMessage, ConfirmTemplate, MessageAction,
    ButtonsTemplate, ImageCarouselTemplate, ImageCarouselColumn, URIAction,
    PostbackAction, DatetimePickerAction,
    CameraAction, CameraRollAction, LocationAction,
    CarouselTemplate, CarouselColumn, PostbackEvent,
    StickerMessage, StickerSendMessage, LocationMessage, LocationSendMessage,
    ImageMessage, VideoMessage, AudioMessage, FileMessage,
    UnfollowEvent, FollowEvent, JoinEvent, LeaveEvent, BeaconEvent,
    FlexSendMessage, BubbleContainer, ImageComponent, BoxComponent,
    TextComponent, SpacerComponent, IconComponent, ButtonComponent,
    SeparatorComponent, QuickReply, QuickReplyButton,
    ImageSendMessage, CarouselContainer)

app = Flask(__name__)
app.secret_key = '213ff3c4dd73d4b04950a'
# app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_host=1, x_proto=1)

# get channel_secret and channel_access_token from your environment variable
channel_secret = os.getenv('LINE_CHANNEL_SECRET', 'f331c146f213ff3c4dd73d4b04950aa0')
channel_access_token = os.getenv('LINE_CHANNEL_ACCESS_TOKEN',
                                 '4xio5wwdgWeIncJoi7rnmLsJTlcEK+7A5k6aEMJ6F3mmpQJVEmCOhL1ocJlDPvkU0YJjI+ga6AkPNTKi3wFhPgHWmD+rz7NWx3P4+UuPlg0Q9wj+yAJKu47rxIwpRVOV9tECXA6XC4e5w9U4YRzfgwdB04t89/1O/w1cDnyilFU=')
if channel_secret is None:
    print('Specify LINE_CHANNEL_SECRET as environment variable.')
    sys.exit(1)
if channel_access_token is None:
    print('Specify LINE_CHANNEL_ACCESS_TOKEN as environment variable.')
    sys.exit(1)

line_bot_api = LineBotApi(channel_access_token)
handler = WebhookHandler(channel_secret)

static_tmp_path = os.path.join(os.path.dirname(__file__), 'static', 'tmp')


# function for create tmp dir for download content
def make_static_tmp_dir():
    try:
        os.makedirs(static_tmp_path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(static_tmp_path):
            pass
        else:
            raise



# [START dialogflow_detect_intent_knowledge]
def detect_intent_knowledge(project_id, session_id, language_code,
                            knowledge_base_id, texts):
    """Returns the result of detect intent with querying Knowledge Connector.

    Args:
    project_id: The GCP project linked with the agent you are going to query.
    session_id: Id of the session, using the same `session_id` between requests
              allows continuation of the conversation.
    language_code: Language of the queries.
    knowledge_base_id: The Knowledge base's id to query against.
    texts: A list of text queries to send.
    """
    import dialogflow_v2beta1 as dialogflow
    session_client = dialogflow.SessionsClient()

    session_path = session_client.session_path(project_id, session_id)
    print('Session path: {}\n'.format(session_path))

    # for text in texts:
    text_input = dialogflow.types.TextInput(
        text=texts, language_code=language_code)

    query_input = dialogflow.types.QueryInput(text=text_input)

    knowledge_base_path = dialogflow.knowledge_bases_client \
        .KnowledgeBasesClient \
        .knowledge_base_path(project_id, knowledge_base_id)

    query_params = dialogflow.types.QueryParameters(
        knowledge_base_names=[knowledge_base_path])

    response = session_client.detect_intent(
        session=session_path, query_input=query_input,
        query_params=query_params)

    print('=' * 20)
    print('Query text: {}'.format(response.query_result.query_text))
    print('Detected intent: {} (confidence: {})\n'.format(
        response.query_result.intent.display_name,
        response.query_result.intent_detection_confidence))
    print('Fulfillment text: {}\n'.format(
        response.query_result.fulfillment_text))
    print('Knowledge results:')
    knowledge_answers = response.query_result.knowledge_answers
    for answers in knowledge_answers.answers:
        print(' - Answer: {}'.format(answers.answer))
        print(' - Confidence: {}'.format(
            answers.match_confidence))

    return response


def detect_intent_texts(project_id, session_id, texts, language_code):
    """Returns the result of detect intent with texts as inputs.

    Using the same `session_id` between requests allows continuation
    of the conversation."""

    import dialogflow_v2 as dialogflow
    session_client = dialogflow.SessionsClient()

    session = session_client.session_path(project_id, session_id)
    print('Session path: {}\n'.format(session))

    # for text in texts:
    text_input = dialogflow.types.TextInput(
        text=texts, language_code=language_code)

    query_input = dialogflow.types.QueryInput(text=text_input)

    response = session_client.detect_intent(
        session=session, query_input=query_input)

    # print('=' * 20)
    # print('Query text: {}'.format(response.query_result.query_text))
    # print('Detected intent: {} (confidence: {})\n'.format(
    #     response.query_result.intent.display_name,
    #     response.query_result.intent_detection_confidence))
    # print('Fulfillment text: {}\n'.format(
    #     response.query_result.fulfillment_text))
    # print('FullFillment Messages')

    # print("DIALGFLOW RESPONSE")
    print(response)

    jsonObj = MessageToJson(response)
    result = json.loads(jsonObj)
    # print("DIALGFLOW RESPONSE")
    # print(result)
    return result



@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:

        # event.source.user_id
        handler.handle(body, signature)
    except LineBotApiError as e:
        print("Got exception from LINE Messaging API: %s\n" % e.message)
        for m in e.error.details:
            print("  %s: %s" % (m.property, m.message))
        print("\n")
    except InvalidSignatureError:
        abort(400)

    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_text_message(event):
    text = event.message.text
    # print("****LANGUAGE DETECTION")
    # print(detect("War doesn't show"))
    # print("****LANGUAGE DETECTION")
    # print(detect("โทรหา"))
    
    text = responses.handleConversion(event)
    try:
       lang=detect(text)
    except:
       lang=responses.getLangCode(event)
    response = detect_intent_texts('drivemate-qupjeb', event.source.user_id, text, lang)


    if 'displayName' in response["queryResult"]["intent"]:
        if response["queryResult"]["intent"]["displayName"] == 'defaultWelcomeIntent':
            message = responses.welcomeResponse(response["queryResult"]["languageCode"])
            line_bot_api.reply_message(
                event.reply_token,
                message
            )
        elif response["queryResult"]["intent"]["displayName"] == 'FAQs':
            print("^^^^^^^^^^^^^^^^^^^^")
            print(response["queryResult"]["languageCode"])
            message = responses.faqs(response["queryResult"]["languageCode"])
            line_bot_api.reply_message(
                event.reply_token,
                message
            )

        elif response["queryResult"]["intent"]["displayName"] == 'quickSearchByLocation':

            print("**** quickSearchByLocation")
            print(response)
            if 'fulfillmentText' in response["queryResult"]:
                if response["queryResult"]['fulfillmentText'] == 'Please specify the rent type.':
                    print(response)
                    print("**** Intent Detected")
                    message = responses.rentTypes(response["queryResult"]["languageCode"])
                    line_bot_api.reply_message(
                        event.reply_token,
                        message
                    )
                elif response["queryResult"]['fulfillmentText'] == 'Please specify the location.':
                    message = responses.locations(response["queryResult"]["languageCode"])
                    line_bot_api.reply_message(
                        event.reply_token,
                        message
                    )
                elif response["queryResult"]['fulfillmentText'] == 'Please specify the start-date.':
                    message = responses.timeduration(response["queryResult"]["languageCode"])
                    line_bot_api.reply_message(
                        event.reply_token,
                        message
                    )
                elif response["queryResult"]['fulfillmentText'] == 'Please specify the end-date.':
                    print("pass")

                if "allRequiredParamsPresent" in response["queryResult"]:
                    [contents_list, num_cars] = queries.searchCarsbyLocation(response, event)
                    list_of_flex = []
                    if num_cars <= 0:
                        line_bot_api.reply_message(
                            event.reply_token, TextSendMessage(
                                "There is no car available in your area at the moment. Please try again later."))
                    elif num_cars > 0:
                        # PUSH CARS INTO DATABASE FOR THAT USER WITH A TIME KEY
                        if num_cars >= 9:
                            counter = 9
                        else:
                            counter = num_cars
                        for i in range(counter):
                            list_of_flex.append(contents_list[i])
                        my_message = CarouselContainer(contents=list_of_flex)
                        message = FlexSendMessage(alt_text="Search Result", contents=my_message)
                        control_message = responses.show_controls(num_cars, 1, counter,event)
                        message_array = [message, control_message]
                        line_bot_api.reply_message(
                            event.reply_token,
                            message_array
                        )



        elif response["queryResult"]["intent"]["displayName"] == 'searchWithPreference':

            print("**** searchWithPreference")
            print(response)
            if 'fulfillmentText' in response["queryResult"]:
                if response["queryResult"]['fulfillmentText'] == 'Please specify the rent type.':
                    print(response)
                    print("**** Intent Detected")
                    message = responses.rentTypes(response["queryResult"]["languageCode"])
                    line_bot_api.reply_message(
                        event.reply_token,
                        message
                    )
                elif response["queryResult"]['fulfillmentText'] == 'Please specify the location.':
                    message = responses.locations(response["queryResult"]["languageCode"])
                    line_bot_api.reply_message(
                        event.reply_token,
                        message
                    )
                elif response["queryResult"]['fulfillmentText'] == 'Please specify the start-date.':
                    message = responses.swptimeduration(response["queryResult"]["languageCode"])
                    line_bot_api.reply_message(
                        event.reply_token,
                        message
                    )
                elif response["queryResult"]['fulfillmentText'] == 'Please specify the end-date.':
                    print("pass")

                elif response["queryResult"]['fulfillmentText'] == 'Please specify your price range preference.':
                    message = responses.prices(response["queryResult"]["languageCode"])
                    line_bot_api.reply_message(
                        event.reply_token,
                        message
                    )

                elif response["queryResult"]['fulfillmentText'] == 'Please specify your brand preference if any.':
                    message = responses.brand_names(response["queryResult"]["languageCode"])
                    line_bot_api.reply_message(
                        event.reply_token,
                        message
                    )

                if "allRequiredParamsPresent" in response["queryResult"]:
                    [contents_list, num_cars] = queries.searchWithPreference(response, event)
                    list_of_flex = []
                    if num_cars <= 0:
                        line_bot_api.reply_message(
                            event.reply_token, TextSendMessage(
                                "There is no car available in your area at the moment. Please try again later."))
                    elif num_cars > 0:
                        # PUSH CARS INTO DATABASE FOR THAT USER WITH A TIME KEY
                        if num_cars >= 9:
                            counter = 9
                        else:
                            counter = num_cars
                        for i in range(counter):
                            list_of_flex.append(contents_list[i])
                        my_message = CarouselContainer(contents=list_of_flex)
                        message = FlexSendMessage(alt_text="Search Result", contents=my_message)
                        control_message = responses.show_controls(num_cars, 1, counter,event)
                        message_array = [message, control_message]
                        line_bot_api.reply_message(
                            event.reply_token,
                            message_array
                        )


        elif response["queryResult"]["intent"]["displayName"] == 'endConversation':
            print("End Conversation")
            line_bot_api.reply_message(
                event.reply_token,
                TextSendMessage(text="Thank you for choosing Drivemate. We are always here to help you. Good Bye! 😊")
            )


        else: # 'isFallback' in response["queryResult"]["intent"]:

            #print("Looking In Knowledge Base")
            #new_response = detect_intent_knowledge('drivemate-qupjeb', event.source.user_id, 'en',
            #                                       'MTc0NjY1ODIyMzQxMDUzODA4NjQ', text)
            #jsonObj = MessageToJson(new_response)
            #result = json.loads(jsonObj)
            #response = "Sorry I did not get that, Can you please try again."
            if 'fulfillmentText' in response['queryResult']:
                resp = response['queryResult']['fulfillmentText']
                # textreply=TextSendMessage(text=response)
                final_Option = responses.finalOptions()

                message_array = [TextSendMessage(text=resp), final_Option]
                line_bot_api.reply_message(
                    event.reply_token,
                    message_array
                    # TextSendMessage(response)
                )
                # line_bot_api.reply_message(
                #     event.reply_token, TextSendMessage(text=response))

    messenger.log(event, text, response)


@handler.add(MessageEvent, message=LocationMessage)
def handle_location_message(event):
    
    #Receive location message from user transform it to test and send it to dialogflow for intent detection and slot filling
    langCode=responses.getLangCode(event)
    #print("**********************************************")
    print(langCode)
    if langCode=='th':
        text = 'ที่ตั้ง ' + 'ละติจูด ' + str(event.message.latitude) + ' ลองจิจูด ' + str(event.message.longitude)
    else:
        text = 'location ' + 'lat ' + str(event.message.latitude) + ' long ' + str(event.message.longitude)
    event.message.type = 'text'
    event.message.text = text
    handle_text_message(event)


@handler.add(MessageEvent, message=StickerMessage)
def handle_sticker_message(event):
    #receive the sticker messages and reply back with what has been received.
    line_bot_api.reply_message(
        event.reply_token,
        StickerSendMessage(
            package_id=event.message.package_id,
            sticker_id=event.message.sticker_id)
    )


# Other Message Type
@handler.add(MessageEvent, message=(ImageMessage, VideoMessage, AudioMessage))
def handle_content_message(event):
    #receive media files and store them to the static path and reply the path back
    if isinstance(event.message, ImageMessage):
        ext = 'jpg'
    elif isinstance(event.message, VideoMessage):
        ext = 'mp4'
    elif isinstance(event.message, AudioMessage):
        ext = 'm4a'
    else:
        return

    message_content = line_bot_api.get_message_content(event.message.id)
    with tempfile.NamedTemporaryFile(dir=static_tmp_path, prefix=ext + '-', delete=False) as tf:
        for chunk in message_content.iter_content():
            tf.write(chunk)
        tempfile_path = tf.name

    dist_path = tempfile_path + '.' + ext
    dist_name = os.path.basename(dist_path)
    os.rename(tempfile_path, dist_path)

    line_bot_api.reply_message(
        event.reply_token, [
            TextSendMessage(text='Image Saved on Server.'),
            TextSendMessage(text=request.host_url + os.path.join('static', 'tmp', dist_name))
        ])


@handler.add(MessageEvent, message=FileMessage)
def handle_file_message(event):
    #recieve the file message and store the file to local directory reply back with the address of stored file.
    message_content = line_bot_api.get_message_content(event.message.id)
    with tempfile.NamedTemporaryFile(dir=static_tmp_path, prefix='file-', delete=False) as tf:
        for chunk in message_content.iter_content():
            tf.write(chunk)
        tempfile_path = tf.name

    dist_path = tempfile_path + '-' + event.message.file_name
    dist_name = os.path.basename(dist_path)
    os.rename(tempfile_path, dist_path)

    line_bot_api.reply_message(
        event.reply_token, [
            TextSendMessage(text='Save file.'),
            TextSendMessage(text=request.host_url + os.path.join('static', 'tmp', dist_name))
        ])


@handler.add(FollowEvent)
def handle_follow(event):
    # when a follow event is sent this function will receive it.
    line_bot_api.reply_message(
        event.reply_token, TextSendMessage(text='Welcome to Drivemate.'))


@handler.add(UnfollowEvent)
def handle_unfollow():
    #when an unfollow event is sent this function will receive it.
    app.logger.info("Got Unfollow event")


@handler.add(JoinEvent)
def handle_join(event):
    #When a join event is sent this function receives the event.
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text='Joined this ' + event.source.type))


@handler.add(LeaveEvent)
def handle_leave():
    # when a leave event is sent this function receives the event.
    app.logger.info("Got leave event")


@handler.add(PostbackEvent)
def handle_postback(event):
    lang=responses.getLangCode(event)
    #When a postback event is sent this function recieves the event and process it.
    #When start date and end date postback events are sent this function received them transforms them and sends to dialogflow for slot filling.
    if event.postback.data == 'startdatetime postback':
        datetimestring = event.postback.params['datetime'].replace('-', '').replace(':', '').replace('T', '')
        datetimestring = datetimestring[2:12]
        if lang=='th':
            postbacktext="เริ่มวันที่และเวลา" +" "+ datetimestring
        else:
            postbacktext = event.postback.data + " " + datetimestring
        id = random.randrange(100000000000, 200000000000000000, 1)
        mymessage = {"id": id, "text": postbacktext, "type": "text"}
        print("*********************MESSAGE")
        newmessage = MessageEvent(timestamp=event.timestamp, source=event.source, reply_token=event.reply_token,
                                  message=mymessage)
        handle_text_message(newmessage)

    elif event.postback.data == 'enddatetime postback':
        datetimestring = event.postback.params['datetime'].replace('-', '').replace(':', '').replace('T', '')
        datetimestring = datetimestring[2:12]
        if lang=='th':
            postbacktext="สิ้นสุดวันที่และเวลา" +" "+ datetimestring
        else:
            postbacktext = event.postback.data + " " + datetimestring

        id = random.randrange(100000000000, 200000000000000000, 1)
        mymessage = {"id": id, "text": postbacktext, "type": "text"}
        newmessage = MessageEvent(timestamp=event.timestamp, source=event.source, reply_token=event.reply_token,
                                  message=mymessage)
        handle_text_message(newmessage)


    elif event.postback.data == 'swp startdatetime postback':
        datetimestring = event.postback.params['datetime'].replace('-', '').replace(':', '').replace('T', '')
        datetimestring = datetimestring[2:12]
        if lang=='th':
            postbacktext="ค้นหาด้วยเวลาเริ่มต้นการตั้งค่า" +" "+ datetimestring
        else:
            postbacktext = event.postback.data + " " + datetimestring

        id = random.randrange(100000000000, 200000000000000000, 1)
        mymessage = {"id": id, "text": postbacktext, "type": "text"}
        print("*********************MESSAGE")
        newmessage = MessageEvent(timestamp=event.timestamp, source=event.source, reply_token=event.reply_token,
                                  message=mymessage)
        handle_text_message(newmessage)

    elif event.postback.data == 'swp enddatetime postback':
        datetimestring = event.postback.params['datetime'].replace('-', '').replace(':', '').replace('T', '')
        datetimestring = datetimestring[2:12]
        if lang=='th':
            postbacktext="ค้นหาด้วยเวลาสิ้นสุดการตั้งค่า" +" "+ datetimestring
        else:
            postbacktext = event.postback.data + " " + datetimestring

        id = random.randrange(100000000000, 200000000000000000, 1)
        mymessage = {"id": id, "text": postbacktext, "type": "text"}
        newmessage = MessageEvent(timestamp=event.timestamp, source=event.source, reply_token=event.reply_token,
                                  message=mymessage)
        handle_text_message(newmessage)


    elif 'Cars Id' in event.postback.data:
        # When a postback event with this key word is sent this function handles the display of more cars.
        responses.showMore(event)


    elif 'View Detail' in event.postback.data:
        #When a postback event with this key word is sent this function handles the display for car details.
        print(event.postback.data)
        responses.viewCarDetail(event)

        print("Please wait while Iam getting the details")


    elif 'Go Back' in event.postback.data:
        #When a postback event with this keyword is received this function handles the display of previous list of search  results.
        print(event.postback.data)
        responses.showCarsHistory(event)
        print("Please wait while Iam getting the details")




    elif 'Book Car Id' in event.postback.data:
        # When a postback event with this keyword is received this fucntion handles booking of the car.
        print(event.postback.data)
        print("Please wait while Iam booking your car")  # FAWZAN GET THE CAR ID AND USER ID HERE AND WRITE A FUNCTION TO HANDLE BOOKING
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(
                text='Please wait im booking your car'))


@handler.add(BeaconEvent)
def handle_beacon(event):
    # When a beacon event is received, this function handles the event and replies back to the messenger.
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(
            text='Got beacon event. hwid={}, device_message(hex string)={}'.format(
                event.beacon.hwid, event.beacon.dm)))


@app.route('/static/<path:path>')
def send_static_content(path):
    # this function handles the uploading of content to static path
    return send_from_directory('static', path)


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        usage='Usage: python ' + __file__ + ' [--port <port>] [--help]'
    )
    arg_parser.add_argument('-p', '--port', type=int, default=8000, help='port')
    arg_parser.add_argument('-d', '--debug', default=False, help='debug')
    options = arg_parser.parse_args()

    # create tmp dir for download content
    make_static_tmp_dir()

    app.run(debug=options.debug, port=options.port)
